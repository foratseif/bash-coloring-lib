#!/bin/bash
# Usage Example: echo -e $(bold 'this is in bold')

export s_bold="\033[1m"
export e_bold="\033[21m"

export s_dim="\033[2m"
export e_dim="\033[22m"

export s_underline="\033[4m"
export e_underline="\033[24m"

export s_blink="\033[5m"
export e_blink="\033[25m"

export s_invert="\033[7m"
export e_invert="\033[27m"

export s_hidden="\033[8m"
export e_hidden="\033[28m"

export s_fg_black="\033[30m"
export e_fg_black="\033[39m"

export s_fg_white="\033[97m"
export e_fg_white="\033[39m"

export s_fg_lgray="\033[37m"
export e_fg_lgray="\033[39m"

export s_fg_dgray="\033[90m"
export e_fg_dgray="\033[39m"

export s_bg_black="\033[40m"
export e_bg_black="\033[49m"

export s_bg_white="\033[107m"
export e_bg_white="\033[49m"

export s_bg_lgray="\033[47m"
export e_bg_lgray="\033[49m"

export s_bg_dgray="\033[100m"
export e_bg_dgray="\033[49m"

export s_fg_red="\033[31m"
export e_fg_red="\033[39m"

export s_fg_lred="\033[91m"
export e_fg_lred="\033[39m"

export s_bg_red="\033[41m"
export e_bg_red="\033[49m"

export s_bg_lred="\033[101m"
export e_bg_lred="\033[49m"

export s_fg_green="\033[32m"
export e_fg_green="\033[39m"

export s_fg_lgreen="\033[92m"
export e_fg_lgreen="\033[39m"

export s_bg_green="\033[42m"
export e_bg_green="\033[49m"

export s_bg_lgreen="\033[102m"
export e_bg_lgreen="\033[49m"

export s_fg_yellow="\033[33m"
export e_fg_yellow="\033[39m"

export s_fg_lyellow="\033[93m"
export e_fg_lyellow="\033[39m"

export s_bg_yellow="\033[43m"
export e_bg_yellow="\033[49m"

export s_bg_lyellow="\033[103m"
export e_bg_lyellow="\033[49m"

export s_fg_blue="\033[34m"
export e_fg_blue="\033[39m"

export s_fg_lblue="\033[94m"
export e_fg_lblue="\033[39m"

export s_bg_blue="\033[44m"
export e_bg_blue="\033[49m"

export s_bg_lblue="\033[104m"
export e_bg_lblue="\033[49m"

export s_fg_magenta="\033[35m"
export e_fg_magenta="\033[39m"

export s_fg_lmagenta="\033[95m"
export e_fg_lmagenta="\033[39m"

export s_bg_magenta="\033[45m"
export e_bg_magenta="\033[49m"

export s_bg_lmagenta="\033[105m"
export e_bg_lmagenta="\033[49m"

export s_fg_cyan="\033[36m"
export e_fg_cyan="\033[39m"

export s_fg_lcyan="\033[96m"
export e_fg_lcyan="\033[39m"

export s_bg_cyan="\033[46m"
export e_bg_cyan="\033[49m"

export s_bg_lcyan="\033[106m"
export e_bg_lcyan="\033[49m"
