#!/usr/bin/env python
# Author: Forat Seif
# E-mail: foratseif@gmail.com
# Description: This program generates a bash code that can be
#              used as a library of text coloring functions in
#              the terminal.

# imports
import sys

# generates bash start and end vars
def gen_vars(name, code, end):
    ret  = "\n"
    ret += "export s_%s=\"\\033[%sm\"\n" % (name, code)
    ret += "export e_%s=\"\\033[%sm\"\n" % (name, end)
    return ret

# generates bash styling function
def gen_styling(name, code):
    return gen_vars(name, code, "2"+code)

# generates bash foreground coloring function
def gen_fg_coloring(name, code):
    return gen_vars("fg_"+name, code, "39")

# generates bash background coloring function
def gen_bg_coloring(name, code):
    return gen_vars("bg_"+name, code, "49")

# generates foreground, background, normal and light coloring functions
def gen_all_colorings(name, code):
    ret  = gen_fg_coloring(name,  "3"+code)
    ret += gen_fg_coloring("l"+name, "9"+code)
    ret += gen_bg_coloring(name,  "4"+code)
    ret += gen_bg_coloring("l"+name, "10"+code)
    return ret

# main
if __name__ == "__main__":
    if len(sys.argv) <= 1:
        filename = "coloring_lib.sh"
    else:
        filename = sys.argv[1]

    with open(filename, 'w+') as f:
        # add shebang
        f.write("#!/bin/bash\n")

        # add usage example in the bash coloring library
        f.write("# Usage Example: echo -e $(bold 'this is in bold')\n")

        # add bash functions
        f.write(gen_styling("bold",      "1"))
        f.write(gen_styling("dim",       "2"))
        f.write(gen_styling("underline", "4"))
        f.write(gen_styling("blink",     "5"))
        f.write(gen_styling("invert",    "7"))
        f.write(gen_styling("hidden",    "8"))

        f.write(gen_fg_coloring("black", "30"))
        f.write(gen_fg_coloring("white", "97"))
        f.write(gen_fg_coloring("lgray", "37"))
        f.write(gen_fg_coloring("dgray", "90"))

        f.write(gen_bg_coloring("black", "40"))
        f.write(gen_bg_coloring("white", "107"))
        f.write(gen_bg_coloring("lgray", "47"))
        f.write(gen_bg_coloring("dgray", "100"))

        f.write(gen_all_colorings("red", "1"))
        f.write(gen_all_colorings("green", "2"))
        f.write(gen_all_colorings("yellow", "3"))
        f.write(gen_all_colorings("blue", "4"))
        f.write(gen_all_colorings("magenta", "5"))
        f.write(gen_all_colorings("cyan", "6"))


