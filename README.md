# Description
This is a text styling and coloring library for bash.

# Install
Running the ```generator.py``` file:
```
  ./generator.py
```
will generate the ```coloring_lib.sh```.

Then just source the ```coloring_lib.sh``` in the ```.bashrc``` file:
```
  if [ -f /path/to/coloring_lib.sh ]; then
    source /path/to/coloring_lib.sh
  fi;
```

# Usage
**Styling:** Start tag looks like this ```$s_STYLING``` where ```STYLING``` is one of the words listed below. End tag is just like the start tag except that it starts with a ```$e_```.

**Coloring:** Start tag looks like this ```$s_fg_COLOR``` or ```$s_bg_COLOR``` where ```bg``` and ```fg``` stands for background or foreground. End tag is just like the start tag except that it starts with a ```$e_```.

# Example
```
  echo -e "${s_bold}this is in bold${e_bold}"
  echo -e "this ${s_fg_black}${s_bold}'word'${e_bold}${e_fg_black} is both bold and black."
  echo -e "${s_bg_red}only one ${s_bold}'bold'${e_bold} word, rest have red backround.${e_bg_red}"
```

# Colors and Stylings

Stylings:
```
  bold
  dim
  underline
  blink
  invert
  hidden
```

Colors (background and foreground):
```
  black    - Black
  white    - White
  lgray    - Light Gray
  dgray    - Dark Gray
  red      - Red
  green    - Green
  yellow   - Yellow
  blue     - Blue
  magenta  - Magenta
  cyan     - Cyan
  lred     - Light Red
  lgreen   - Light Green
  lyellow  - Light Yellow
  lblue    - Light Blue
  lmagenta - Light Magenta
  lcyan    - Light Cyan
```
